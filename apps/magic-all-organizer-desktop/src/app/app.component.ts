import { Component } from '@angular/core';
import {exampleProducts, Product} from "@dieate/products";

@Component({
  selector: 'dieate-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'magic-all-organizer-desktop';
  products: Product[] = exampleProducts;

}
