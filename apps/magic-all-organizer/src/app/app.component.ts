import { Component } from '@angular/core';

@Component({
  selector: 'dieate-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'magic-all-organizer';
}
